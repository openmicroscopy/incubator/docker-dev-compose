#!/usr/bin/env python

import socket
import sys
import time


s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
host = 'omero-rw'

# Wait up to 10 mins
stop = time.time() + 600
while time.time() < stop:
    time.sleep(5)
    if s.connect_ex((host, 4064)) == 0:
        print('Connected')
        sys.exit(0)
    sys.stdout.write('.')
    sys.stdout.flush()

print('Failed to connect after 10 minutes')
sys.exit(1)
