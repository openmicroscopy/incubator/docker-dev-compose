BEGIN;
CREATE USER postgresro PASSWORD 'postgresro';
GRANT TEMPORARY ON DATABASE postgres TO postgresro;
-- Grant future privileges https://dba.stackexchange.com/a/91974
ALTER DEFAULT PRIVILEGES FOR ROLE postgres IN SCHEMA public GRANT SELECT ON TABLES TO postgresro;
COMMIT;
