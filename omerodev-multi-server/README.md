# omerodev-multi-server

OMERO read-write and read-only dev build servers.

Note PostgresQL users and passwords are hard-coded.

Ports:

- 4065: OMERO.server read-write 4064
- 4064: OMERO.server read-only 4064
- 4080: OMERO.web standalone (4080)
