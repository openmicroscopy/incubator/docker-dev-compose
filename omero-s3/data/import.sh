#!/bin/sh
OMERO=/opt/omero/server/OMERO.server/bin/omero

$OMERO login -C -slocalhost -uroot -womero
$OMERO import -T Dataset:name:s3 --debug DEBUG "$@"
