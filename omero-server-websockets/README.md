# OMERO.server websockets with SSL

This is an example of running OMERO.server with websockets, Nginx, and SSL.

This requires a server built with
- https://github.com/openmicroscopy/openmicroscopy/pull/5927
- https://github.com/openmicroscopy/openmicroscopy/pull/5998

Copy your `OMERO.server-VERSION-ice36-BUILD.zip` to `server/OMERO.server.zip`, then run `docker-compose build`.


## Example client script

[`example.py`](`example.py`) is an example python script that can be used to play with IceSSL options.
See the file for details.

This requires a client built with https://github.com/openmicroscopy/openmicroscopy/pull/5997


## Certificates

`icessl/` contains a test CA and server certificates for testing.
You can generate a new set of certificate by running `ice-ca-certs.py [CommonName]` which uses the [Zeroc Ice Certificate Utility](https://pypi.org/project/zeroc-icecertutils/) to create a new authority and server certificate.
For convenience a `Dockerfile` is provided.

- `cacert.jks`: CA truststore, used by Ice Java to verify the server certificate
- `cacert.pem`: CA certificate, used by Ice Python and C++ to verify the server certificate
- `server.pem`, `server.key`: Server certificate and key, used by Nginx for https
- `server.p12`: Server certificate and key in PKCS12 format with password `secret`, used by Ice servers

### Real certificates

If you have a certificate from a commercial certificate authority you can convert it to the PKCS12 format:

    openssl pkcs12 -export -out server.p12 -in server.pem -inkey server.key -passout pass:secret


## Secure client connections

OMERO clients do not verify the host they are connecting to, which means they're vulnerable to a man-in-the-middle attack.

On the server side You can improve security by setting stricter IceSSL server options, for instance by replacing insecure anonymous Diffie Hellman ciphers and requiring TLS 1.2.
See `docker-compose.sslverify.yml` for an example:

    docker-compose -f docker-compose.yml -f docker-compose.sslverify.yml up -d

OMERO clients require additional IceSSL settings to connect to this server, and to ensure they verify the server certificate before connecting.
Examples of the changes required are:
- Python: https://github.com/manics/openmicroscopy/tree/sslverify
- Java: https://github.com/manics/omero-blitz/tree/sslverify


## SELinux

If SELinux is enabled you may have problems reading host mounted volumes or files.
Set `VOLOPTS=,z` in `.env`.

## References

- https://doc.zeroc.com/technical-articles/glacier2-articles/teach-yourself-glacier2-in-10-minutes#TeachYourselfGlacier2in10Minutes-UsingSSLwithGlacier2
- https://doc.zeroc.com/ice/3.7/ice-plugins/icessl/configuring-icessl
- https://doc.zeroc.com/ice/3.7/ice-plugins/icessl/setting-up-a-certificate-authority
- https://doc.zeroc.com/ice/3.7/property-reference/icessl
