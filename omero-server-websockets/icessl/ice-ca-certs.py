#!/usr/bin/env python
# Create self-signed certificates for testing.
# Requires https://pypi.org/project/zeroc-icecertutils/
# Optional arg: CommonName for certificate

import IceCertUtils
import sys

try:
    cn = sys.argv[1]
except IndexError:
    cn = "localhost"
# Password for JKS and P12 files
P12_PASSWORD = "secret"

# Create the certicate factory
factory = IceCertUtils.CertificateFactory(cn = "My CA")

# Get the CA certificate and save as JKS and PEM
ca = factory.getCA()
ca.save("cacert.jks", password=P12_PASSWORD)
ca.save("cacert.pem")
ca.saveKey("cacert.key")

# Create a client certificate, save as PKCS12
#client = factory.create("client", cn = "Client")
#client.save("client.p12", password=PASSWORD)
# Save to JKS format and also include the CA certificate with alias "cacert"
#client.save("client.jks", caalias="cacert", password=PASSWORD)

# Create the server certificate
server = factory.create("server", cn=cn)
# Save as PKCS12 and pem
server.save("server.p12", password=P12_PASSWORD)
server.save("server.pem")
server.saveKey("server.key")

factory.destroy()
