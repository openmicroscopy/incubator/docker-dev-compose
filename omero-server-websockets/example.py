#!/usr/bin/env python

# Args: protocol://host:port/prefix username password
# Eg:
#   wss direct to OMERO.server:
#     ./example.py wss://localhost:4066/omero-wss root omero
#   wss to Nginx, wss proxied to OMERO.server:
#     ./example.py wss://localhost:8443/omero-wss root omero

import sys
import omero.clients
from omero.rtypes import unwrap

# Use this to play with IceSSL client args, e.g. to verify certificates
# and/or hostnames
iceargs = {
    # 'Ice.Trace.Network': '3',
    # 'Ice.Trace.Protocol': '1',
    # 'IceSSL.Trace.Security': '1',
    # 'IceSSL.VerifyPeer': '1',
    # 'IceSSL.UsePlatformCAs': '1',
    # 'IceSSL.CheckCertName': '1',
    'IceSSL.Ciphers': 'ALL',  # Nginx needs HIGH
    # 'IceSSL.Protocols': 'tls1_2',
}

host = sys.argv[1]
user = sys.argv[2]
password = sys.argv[3]
c = omero.client(host=host, args=['--%s=%s' % a for a in iceargs.items()])
s = c.createSession(user, password)
print('Connected: {}'.format(s))
for user in unwrap(s.getQueryService().projection("""
    SELECT id, omeName, firstName, lastName FROM Experimenter
        """, None)):
    print(user)
c.closeSession()
print('Disconnected')
