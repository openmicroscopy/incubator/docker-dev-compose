# omero-samba-server

OMERO and SAMBA servers for testing imports.

Build and start containers:

    docker-compose up -d
    docker-compose logs -f

Create a file in the samba share and mount:

    docker-compose exec samba touch /srv/hello.fake
    docker-compose exec -u0 omero mount -t cifs -o username=user,password=password,vers=3.0 //samba/data /mnt

Run an in-place import:

    docker-compose exec omero /opt/omero/server/OMERO.server/bin/omero -slocalhost -uroot -womero import --transfer=ln_s /mnt/hello.fake
