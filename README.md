# OMERO dev docker-compose files

A collection of docker-compose setups for testing and developing OMERO.server.
See the READMEs in each directory.
