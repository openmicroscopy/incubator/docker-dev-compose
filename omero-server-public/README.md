# omero public web server

OMERO with public web.

Note PostgresQL users and passwords are hard-coded.

Ports:
- 4064: OMERO.server 4064
- 4080: OMERO.web standalone (4080)


# Example
```
docker-compose exec omero touch /tmp/image.fake
docker-compose exec omero /opt/omero/server/OMERO.server/bin/omero import -s localhost -u root -w omero -T Dataset:name:test /tmp/image.fake
```
